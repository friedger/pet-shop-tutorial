window.blockstack = blockstack;

App = {
  web3Provider: null,
  contracts: {},

  init: function () {
    // Load pets.
    $.getJSON('../pets.json', function (data) {
      var petsRow = $('#petsRow');
      var petTemplate = $('#petTemplate');

      for (i = 0; i < data.length; i++) {
        petTemplate.find('.panel-title').text(data[i].name);
        petTemplate.find('img').attr('src', data[i].picture);
        petTemplate.find('.pet-breed').text(data[i].breed);
        petTemplate.find('.pet-age').text(data[i].age);
        petTemplate.find('.pet-location').text(data[i].location);
        petTemplate.find('.btn-like').attr('data-id', data[i].id);
        petTemplate.find('.btn-adopt').attr('data-id', data[i].id);

        petsRow.append(petTemplate.html());
      }
    });

    return App.initWeb3();
  },

  initWeb3: function () {
    // Is there is an injected web3 instance?
    if (typeof web3 !== 'undefined') {
      App.web3Provider = web3.currentProvider;
    } else {
      // If no injected web3 instance is detected, fallback to the TestRPC
      App.web3Provider = new Web3.providers.HttpProvider('http://localhost:8545');
    }
    web3 = new Web3(App.web3Provider);


    return App.initContract();
  },

  initContract: function () {
    $.getJSON('Adoption.json', function (data) {
      // Get the necessary contract artifact file and instantiate it with truffle-contract
      var AdoptionArtifact = data;
      App.contracts.Adoption = TruffleContract(AdoptionArtifact);

      // Set the provider for our contract
      App.contracts.Adoption.setProvider(App.web3Provider);

      // Use our contract to retrieve and mark the adopted pets
      return App.markAdopted();
    });

    return App.bindEvents();
  },

  bindEvents: function () {
    $(document).on('click', '.btn-adopt', App.handleAdopt);
    $(document).on('click', '.btn-like', App.handleLike);
  },

  markAdopted: function () {
    var adoptionInstance;

    App.contracts.Adoption.deployed().then(function (instance) {
      adoptionInstance = instance;

      return adoptionInstance.getAdopters.call();
    }).then(function (adopters) {
      for (i = 0; i < adopters.length; i++) {
        if (adopters[i] !== '0x0000000000000000000000000000000000000000') {
          $('.panel-pet').eq(i).find('button').text('Success').attr('disabled', true);
        }
      }
    }).catch(function (err) {
      console.log(err.message);
    });
  },

  handleAdopt: function () {
    event.preventDefault();

    var petId = parseInt($(event.target).data('id'));
    
    var adoptionInstance;

    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];

      App.contracts.Adoption.deployed().then(function (instance) {
        adoptionInstance = instance;

        // Execute adopt as a transaction by sending account
        return adoptionInstance.adopt(petId, { from: account });
      }).then(function (result) {
        return App.markAdopted();
      }).catch(function (err) {
        console.log(err.message);
      });
    });
  },

  handleLike: function () {
    event.preventDefault();
    var petId = parseInt($(event.target).data('id'));
    var likeAction;
    if (event.target.classList.contains("fa-heart")) {
      event.target.classList.add("fa-heart-o");
      event.target.classList.remove("fa-heart");
      likeAction = false;
    } else {
      event.target.classList.add("fa-heart");
      event.target.classList.remove("fa-heart-o");
      likeAction = true;
    }

    if (likeAction) {
      console.log("I like pet " + petId);
    } else {
      console.log("I don't like pet " + petId + " anymore");
    }
    
    var adoptionInstance;

    web3.eth.getAccounts(function (error, accounts) {
      if (error) {
        console.log(error);
      }

      var account = accounts[0];
      AppUser.handleLikeForAccount(petId, account, likeAction);
    });
  }
};

AppUser = {
  init: function () {
    if (blockstack.isUserSignedIn()) {
      console.log("user signed in");
      var profile = blockstack.loadUserData().profile;
      AppUser.showProfile(profile);
    } else if (blockstack.isSignInPending()) {
      console.log("user signed in pending");
      blockstack.handlePendingSignIn("http://localhost:6270/v1/names/").then(function (userData) {
        window.location = window.location.origin;
      });
    } else {
      console.log("user not yet signed in");
      AppUser.showLogin();
    }

    return AppUser.bindEvents();
  },

  bindEvents: function () {
    $(document).on('click', '#signin-button', AppUser.handleSignin);
    $(document).on('click', '#signout-button', AppUser.handleSignout);
  },

  handleSignin: function () {
    console.log("redirect to sign in");
    event.preventDefault();
    blockstack.redirectToSignIn();
  },

  handleSignout: function () {
    console.log("signed out");
    event.preventDefault();
    blockstack.signUserOut(window.location.href);
    AppUser.showLogin();
  },

  showProfile: function (profile) {
    var person = new blockstack.Person(profile);
    var profileContainer = $(document).find('.site-wrapper-inner');

    profileContainer.find('#heading-name')[0].innerHTML = person.name() ? person.name() : "Nameless Person";
    if (person.avatarUrl()) {
      profileContainer.find('#avatar-image')[0].setAttribute('src', person.avatarUrl());
    }
    profileContainer.find('#section-1')[0].style.display = 'none';
    profileContainer.find('#section-2')[0].style.display = 'block';
    profileContainer.find('#section-2')[0].classList.remove('hide');

    AppUser.loadLikedPets().then(function(likedPetsString) {
      
      var likedPets = JSON.parse(likedPetsString || {});
      var petsRow = $('#petsRow');
      var buttons = petsRow.children().find(".btn-like");
      for (var petId in likedPets) {
        if( likedPets.hasOwnProperty(petId) ) {
          var likeBtn = buttons[petId];
          likeBtn.classList.add("fa-heart");
          likeBtn.classList.remove("fa-heart-o");
        } 
      }         
    }, function(error) {console.trace(error);});
  },

  showLogin: function () {
    var profileContainer = $(document).find('.site-wrapper-inner');
    profileContainer.find('#section-1')[0].style.display = 'block';
    profileContainer.find('#section-2')[0].style.display = 'none';
    profileContainer.find('#section-2')[0].classList.add('hide');
  },

  myPets: {},
  myPetsLoaded: false,
  petfile: "liked-pets.json",

  handleLikeForAccount: function (petId, account, likeAction) {
    if (blockstack.isUserSignedIn()) {
      if (!AppUser.myPetsLoaded) {
        blockstack.getFile(AppUser.petfile, false).then(function (data) {
          console.log(data);
          if (data === null) {
            AppUser.myPets = {};
          } else {
            AppUser.myPets = JSON.parse(data || {});
          }
          AppUser.myPetsLoaded = true;
          AppUser.storeLikedPets(petId, account, likeAction);
        });
      } else {
        AppUser.storeLikedPets(petId, account, likeAction);
      }
    } else {
      blockstack.redirectToSignIn();
      // TODO remember this like action for after signin. 
    }
  },

  storeLikedPets: function (petId, account, likeAction) {
    if (likeAction) {
      AppUser.myPets[petId] = account;
    } else {
      delete AppUser.myPets[petId];
    }
    blockstack.putFile(AppUser.petfile, JSON.stringify(AppUser.myPets), false);
  },

  loadLikedPets: function() {
    return blockstack.getFile(AppUser.petfile, false);
  }

};

$(function () {
  $(window).load(function () {
    App.init();
    AppUser.init();
  });
});