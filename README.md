# Truffle's Pet Shop Tutorial with Blockstack #

This tutorial shows how to use Truffle and Blockstack together.

It might help developers to get into distributed app development.

While the truffle app provides a public state of the shop, the addition of blockstack allows to personalize the experience in a decentralized way across devices.

As preparation, please 
* walk through the [Truffle Pet shop tutorial](http://truffleframework.com/tutorials/pet-shop) to understand truffle and the setup for truffle.
* walk through the [Blockstack Todos tutorial](https://blockstack.org/tutorials/todo-list) to understand blockstack and the setup of the testing environment.

** This is for blockstack 0.18.1 **

![personalized view](docs/personalizedview.png)

### How do I get set up? ###

* Launch the blockstack test environment from the todo list repository using `docker-compose up -d`
* launch the test network for etherium `testrpc`
* checkout this repo
* run `npm install`
* run `npm run dev`
* open internet browser localhost:3000, use advanced mode, enter `blockstack-integration-test-api-password`, store the pass phrases of your new blockstack account.
* transfer bitcoins from core wallet to your browser wallet using localhost:8888/wallet/send-core
* buy a username, edit your profile
* in the pet shop app, adopt an animal, like animals.

* Do the same thing in an incognito tab of the browser to see your personalized pet shop (with the likes that you did on the other tab).

### Contribution guidelines ###

* Feel free to open issues and send pull requests.

### Who do I talk to? ###

* You can reach me on gitter.im as @friedger or @friedger@social.nasqueron.org or @fmdroid@twitter.com or @friedger@matrix.org